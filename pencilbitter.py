import dearpygui.dearpygui as dpg
import MainWindow

dpg.create_context()
mainwindow = MainWindow.MainWindow()

dpg.create_viewport(title='Pencil Bitter', width=800, height=600)
dpg.setup_dearpygui()
dpg.show_viewport()
dpg.set_primary_window("Main Window", True)
dpg.start_dearpygui()
dpg.destroy_context()