from mimetypes import init
import dearpygui.dearpygui as dpg
import jsonpickle
from NewProjectWindow import NewProjectWindow
from PaletteWindow import PaletteWindow
from FrameWindow import FrameWindow
from PreviewWindow import PreviewWindow
from Project import Project
from TimeLineWindow import TimeLineWindow
from PillowImageManipulator import PillowImageManipulator

class MainWindow():

    def createMenuBar(self):
        with dpg.menu_bar():
            with dpg.menu(label="Sprite"):
                dpg.add_menu_item(label="New", callback=lambda x:dpg.show_item("new_project_modal"))
                dpg.add_menu_item(label="Open", callback=lambda: dpg.show_item("file_dialog_open"))
                dpg.add_menu_item(label="Save",callback=self.saveProjectMenuCallback)
                dpg.add_menu_item(label="Save As", callback=lambda: dpg.show_item("file_dialog_save"))
                with dpg.menu(label="Import"):
                        dpg.add_menu_item(label="Import Frame", callback=lambda: dpg.show_item("file_dialog_import_image"))
                        dpg.add_menu_item(label="Import SpriteSheet", callback=lambda: dpg.show_item("file_dialog_import_spritesheet"))
                with dpg.menu(label="Export"):
                        dpg.add_menu_item(label="Export Frame", callback=lambda: dpg.show_item("file_dialog_export_image"))
                        dpg.add_menu_item(label="Export SpriteSheet", callback=lambda: dpg.show_item("file_dialog_export_spritesheet"))
                dpg.add_menu_item(label="Exit", callback=lambda x: exit())
            with dpg.menu(label="View"):
                dpg.add_menu_item(label="Show Palette Window")
                dpg.add_menu_item(label="Show TimeLine Window")

    def newProjectCreated(self, width:int, height:int):
        self.currentProject = Project(width,height)
        self.createWindows()
    
    def projectLoaded(self, project: Project):
        self.currentProject = project
        self.createWindows()

    def createWindows(self):
        self.frameWindow = FrameWindow(self.currentProject)
        self.paletteWindow = PaletteWindow(self.currentProject, self.frameWindow.updatePalette)  
        self.timelineWindow = TimeLineWindow(self.currentProject, self.updateFrame)
        self.previewWindow = PreviewWindow(self.currentProject, self.updateWindows)    
    
    def saveProjectMenuCallback(self):
        if self.currentProject.filePath != "":
                self.saveProject()
        else:
                dpg.show_item("file_dialog_save")

    def saveProjectFileDialogCallback(self,sender, app_data):
        self.currentProject.filePath = app_data['file_path_name']
        self.saveProject()

    def saveProject(self):
        filepath = self.currentProject.filePath
        jsonString = jsonpickle.encode(self.currentProject)
        with open(filepath,"w") as f:
                f.write(jsonString)
                f.close()

    def loadProjectFileDialogCallback(self,sender, app_data):
        with open(app_data['file_path_name'],"r") as f:
                jsonString = f.read()
                f.close()
                project = jsonpickle.decode(jsonString)
                self.projectLoaded(project)

    def importImageFileDialogCallback(self, sender, app_data):
        imageManipulator = PillowImageManipulator()
        project = imageManipulator.importImage(app_data['file_path_name'])
        self.currentProject = project
        self.createWindows()
        print("ImageImported")

    def importSpriteSheetFileDialogCallback(self, sender, app_data):
        print("SpriteSheetImported")

    def exportImageFileDialogCallback(self, sender, app_data):
        dpg.save_image(file=app_data['file_path_name'], width=self.currentProject.currentFrame.width, height=self.currentProject.currentFrame.height, data=self.currentProject.currentFrame.getTransformedTextureDataForSaving(self.currentProject.currentPalette), components=4)
    
    def exportSpriteSheetFileDialogCallback(self, sender, app_data):
        imageManipulator = PillowImageManipulator()
        imageManipulator.exportSpritesheet(app_data['file_path_name'],self.currentProject)
        print("SpriteSheetExported")
    
    def updateFrame(self):
        self.frameWindow.updateFrame()
        self.previewWindow.updateFrame(self.currentProject.currentAnimation.currentFrameIndex)
    
    def updateWindows(self):
        self.timelineWindow.updateTimeline()
        self.frameWindow.updateFrame()

    def createMainWindow(self):
        with dpg.window(label="Main Window", tag="Main Window"):
            self.newProjectWindow = NewProjectWindow(self.newProjectCreated)
            self.currentProject = None
            self.paletteWindow = None
            self.frameWindow = None
            self.timelineWindow = None
            self.createMenuBar()
            with dpg.file_dialog(directory_selector=False, show=False,callback=self.saveProjectFileDialogCallback, tag="file_dialog_save", height=300):
                dpg.add_file_extension(".sprite")
            with dpg.file_dialog(directory_selector=False, show=False,callback=self.loadProjectFileDialogCallback, tag="file_dialog_open", height=300):
                dpg.add_file_extension(".sprite")
            with dpg.file_dialog(directory_selector=False, show=False,callback=self.importImageFileDialogCallback, tag="file_dialog_import_image", height=300):
                dpg.add_file_extension("Image files (*.png *.jpg *.bmp){.png,.jpg,.bmp}")
            with dpg.file_dialog(directory_selector=False, show=False,callback=self.importSpriteSheetFileDialogCallback, tag="file_dialog_import_spritesheet", height=300):
                dpg.add_file_extension("Image files (*.png *.jpg *.bmp){.png,.jpg,.bmp}")
            with dpg.file_dialog(directory_selector=False, show=False,callback=self.exportImageFileDialogCallback, tag="file_dialog_export_image", height=300):
                dpg.add_file_extension("Image files (*.png *.jpg *.bmp){.png,.jpg,.bmp}")
            with dpg.file_dialog(directory_selector=False, show=False,callback=self.exportSpriteSheetFileDialogCallback, tag="file_dialog_export_spritesheet", height=300):
                dpg.add_file_extension("Image files (*.png *.jpg *.bmp){.png,.jpg,.bmp}")
            
    def __init__(self) -> None:
        self.createMainWindow()

