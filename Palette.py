import random

class Palette():
    def setDefaultPalette(self):
        self.paletteData = []
        #purple #0
        self.paletteData.append(3)
        self.paletteData.append(0)
        self.paletteData.append(3)
        self.paletteData.append(3)
        #black #1
        self.paletteData.append(0)
        self.paletteData.append(0)
        self.paletteData.append(0)
        self.paletteData.append(3)
        #white #2
        self.paletteData.append(3)
        self.paletteData.append(3)
        self.paletteData.append(3)
        self.paletteData.append(3)
        #red #3
        self.paletteData.append(3)
        self.paletteData.append(0)
        self.paletteData.append(0)
        self.paletteData.append(3)
        #orange? #4
        self.paletteData.append(3)
        self.paletteData.append(3)
        self.paletteData.append(0)
        self.paletteData.append(3)
        #green #5
        self.paletteData.append(0)
        self.paletteData.append(3)
        self.paletteData.append(0)
        self.paletteData.append(3)
        #celeste #6
        self.paletteData.append(0)
        self.paletteData.append(3)
        self.paletteData.append(3)
        self.paletteData.append(3)
        #blue #7
        self.paletteData.append(0)
        self.paletteData.append(0)
        self.paletteData.append(3)
        self.paletteData.append(3)
        #lightgrey #8
        self.paletteData.append(2)
        self.paletteData.append(2)
        self.paletteData.append(2)
        self.paletteData.append(3)
        #midred #9
        self.paletteData.append(2)
        self.paletteData.append(0)
        self.paletteData.append(0)
        self.paletteData.append(3)
        # ?? #10
        self.paletteData.append(2)
        self.paletteData.append(1)
        self.paletteData.append(0)
        self.paletteData.append(3)
        # ?? #11
        self.paletteData.append(2)
        self.paletteData.append(1)
        self.paletteData.append(1)
        self.paletteData.append(3)
        # midpurple #12
        self.paletteData.append(2)
        self.paletteData.append(0)
        self.paletteData.append(2)
        self.paletteData.append(3)
        #darkgrey #13
        self.paletteData.append(1)
        self.paletteData.append(1)
        self.paletteData.append(1)
        self.paletteData.append(3)
        #darkred #14
        self.paletteData.append(1)
        self.paletteData.append(0)
        self.paletteData.append(0)
        self.paletteData.append(3)
        #darkpurple #15
        self.paletteData.append(1)
        self.paletteData.append(0)
        self.paletteData.append(1)
        self.paletteData.append(3)

    def getColorForDearPyGui(self, colorIndex:int=-1) -> tuple:
        if(colorIndex == -1):
            colorIndex = self.currentColorIndex
        color = (self.paletteData[colorIndex*4]*85, self.paletteData[(colorIndex*4)+1]*85, self.paletteData[(colorIndex*4)+2]*85, self.paletteData[(colorIndex*4)+3]*85) 
        return color
    
    def updateColor(self, colorIndex:int, channel:int, value:int):
        self.paletteData[(colorIndex*4)+channel] = value 
    
    def getColorComponent(self, colorIndex:int, channel:int) -> int:
        return self.paletteData[(colorIndex*4)+channel]

    def __init__(self) -> None:
        self.setDefaultPalette()
        self.currentColorIndex = 2

