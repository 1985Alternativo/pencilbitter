import dearpygui.dearpygui as dpg
from Project import Project

class  TimeLineWindow():

    def addFrameToTimeLine(self):
        frame = self.currentProject.currentAnimation.addFrame()
        index = len(self.currentProject.currentAnimation.frameList) - 1
        with dpg.texture_registry(show=False):
                try:
                    dpg.set_value("frame_image_"+str(index), frame.getTransformedTextureData(self.currentProject.currentPalette))
                except:
                    print("dynamic texture don't exist yet, creating")
                    dpg.add_dynamic_texture(frame.width, frame.height, frame.getTransformedTextureData(self.currentProject.currentPalette), tag="frame_image_"+str(index))
        dpg.add_image_button("frame_image_"+str(index),width=self.currentProject.currentFrame.width*2,height=self.currentProject.currentFrame.height*2,parent=self.frameGroup, callback=self.selectFrame, user_data=index, tag="frame_button_"+str(index))
    
    def cloneFrameInTimeLine(self):
        frame = self.currentProject.currentAnimation.cloneCurrentFrame()
        index = len(self.currentProject.currentAnimation.frameList) - 1 
        with dpg.texture_registry(show=False):
                dpg.add_dynamic_texture(frame.width, frame.height, frame.getTransformedTextureData(self.currentProject.currentPalette), tag="frame_image_"+str(index))
        self.updateFrameImages()
        dpg.add_image_button("frame_image_"+str(index),width=self.currentProject.currentFrame.width*2,height=self.currentProject.currentFrame.height*2,parent=self.frameGroup, callback=self.selectFrame, user_data=index, tag="frame_button_"+str(index))

    def deleteFrameInTimeLine(self):
        frameToDelete = self.currentProject.currentAnimation.getCurrentFrame()
        lastIndex = len(self.currentProject.currentAnimation.frameList) - 1
        self.currentProject.currentAnimation.frameList.remove(frameToDelete)
        self.updateFrameImages()
        dpg.delete_item("frame_button_"+str(lastIndex))
        if(self.currentProject.currentAnimation.getCurrentFrameIndex()<lastIndex):
                self.selectFrame("","",self.currentProject.currentAnimation.getCurrentFrameIndex())
        else:
                if(lastIndex>0):
                        self.selectFrame("","",lastIndex-1)

    def selectFrame(self, sender, app_data, user_data):
        self.currentProject.currentAnimation.currentFrameIndex = user_data
        self.currentProject.currentFrame = self.currentProject.currentAnimation.getCurrentFrame()
        self.updateFrameListener()
    
    def createFrameImages(self):
        with dpg.texture_registry(show=False):
                for index, frame in enumerate(self.currentProject.currentAnimation.frameList):
                        dpg.add_dynamic_texture(frame.width, frame.height, frame.getTransformedTextureData(self.currentProject.currentPalette), tag="frame_image_"+str(index))
    
    def updateFrameImages(self):
        with dpg.texture_registry(show=False):
                for index, frame in enumerate(self.currentProject.currentAnimation.frameList):
                        try:
                            dpg.set_value("frame_image_"+str(index), frame.getTransformedTextureData(self.currentProject.currentPalette))
                        except:
                            print("dynamic texture don't exist yet, creating")
                            dpg.add_dynamic_texture(frame.width, frame.height, frame.getTransformedTextureData(self.currentProject.currentPalette), tag="frame_image_"+str(index))

    def createTimeLine(self):
            for index,frame in enumerate(self.currentProject.currentAnimation.frameList):
                dpg.add_image_button("frame_image_"+str(index),width=self.currentProject.currentFrame.width*2,height=self.currentProject.currentFrame.height*2,callback=self.selectFrame, user_data=index, tag="frame_button_"+str(index), parent=self.frameGroup)

    def updateTimeline(self):
        dpg.delete_item(self.frameGroup,children_only=True)
        self.updateFrameImages()
        self.createTimeLine()

    def createTimeLineWindow(self):
        heightwindow = dpg.get_item_height("frame_window")
        heightwindow = 500 #fix
        with dpg.window(label="TimeLine", width=700,pos=(0,heightwindow)):
            with dpg.menu_bar():
                dpg.add_button(label="+",callback=self.addFrameToTimeLine)
                dpg.add_button(label="x",callback=self.deleteFrameInTimeLine)
                dpg.add_button(label="oo", callback=self.cloneFrameInTimeLine)
            with dpg.group(horizontal=True) as self.frameGroup:
                self.createTimeLine()
    
    def __init__(self,currentProject:Project,updateFrameListener) -> None:
        self.currentProject = currentProject
        self.createFrameImages()
        self.createTimeLineWindow()
        self.updateFrameListener = updateFrameListener
        