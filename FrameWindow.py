import dearpygui.dearpygui as dpg

from Palette import Palette
from Project import Project
from Frame import Frame
from PillowImageManipulator import PillowImageManipulator

class FrameWindow():

    def createGridLayer(self):
        with dpg.draw_layer(parent=self.frameDrawList) as self.grid_layer:
            for j in range(0,self.currentProject.currentFrame.height):
                    dpg.draw_line((0, j*self.currentZoom), (self.currentProject.currentFrame.width*self.currentZoom,j*self.currentZoom), color=(255, 255, 255, 255), thickness=1)
            for i in range(0,self.currentProject.currentFrame.width):
                    dpg.draw_line((i*self.currentZoom, 0), (i*self.currentZoom, self.currentProject.currentFrame.height*self.currentZoom), color=(255, 255, 255, 255), thickness=1)
        with dpg.draw_layer(parent=self.frameDrawList) as self.frame_grid_layer:
            for j in range(0,int(self.currentProject.currentFrame.height/8)):
                    dpg.draw_line((0, j*self.currentZoom*8), (self.currentProject.currentFrame.width*self.currentZoom,j*self.currentZoom*8), color=(0, 0, 0, 255), thickness=2)
            for i in range(0,int(self.currentProject.currentFrame.width/8)):
                    dpg.draw_line((i*self.currentZoom*8, 0), (i*self.currentZoom*8, self.currentProject.currentFrame.height*self.currentZoom), color=(0, 0, 0, 255), thickness=2)
    
    def drawImage(self):
        with dpg.draw_layer(parent=self.frameDrawList):
                #dpg.draw_image(self.frameTexture,(0,0),(self.currentProject.currentFrame.width*self.currentZoom,self.currentProject.currentFrame.height*self.currentZoom))
                dpg.draw_image("frame_image_current",(0,0),(self.currentProject.currentFrame.width*self.currentZoom,self.currentProject.currentFrame.height*self.currentZoom))
    def drawCursor(self,x: int,y: int):
        with dpg.draw_layer(parent=self.frameDrawList) as self.over_layer:
            with dpg.draw_node(tag="cursor_node_tag"):
                dpg.draw_polygon(points=[(x*self.currentZoom,y*self.currentZoom),((x+1)*self.currentZoom,y*self.currentZoom),((x+1)*self.currentZoom,(y+1)*self.currentZoom),(x*self.currentZoom,(y+1)*self.currentZoom)],color=(255, 255, 255, 255),fill=(255, 255, 255, 255), tag="cursor_tag")

    def drawPoint(self):
        (x,y) = dpg.get_drawing_mouse_pos()
        x = int(x/self.currentZoom)
        y = int(y/self.currentZoom)
        self.currentProject.currentFrame.setPixel(x,y,self.currentProject.currentPalette.currentColorIndex)
        dpg.set_value("frame_image_"+str(self.currentProject.currentAnimation.getCurrentFrameIndex()), self.currentProject.currentFrame.getTransformedTextureData(self.currentProject.currentPalette))
        #exporter = PillowImageManipulator()
        #scaledData = exporter.scaleFrameForDearPyGui(self.currentProject.currentFrame,self.currentProject.currentPalette,self.currentZoom)
        dpg.set_value("frame_image_current", self.currentProject.currentFrame.getTransformedTextureData(self.currentProject.currentPalette))

    def updateContents(self):
        dpg.delete_item("frame_draw_list",children_only=True)
        dpg.configure_item("frame_draw_list", width=self.currentProject.currentFrame.width*self.currentZoom, height=self.currentProject.currentFrame.height*self.currentZoom)
        self.drawImage()
        self.createGridLayer()
        self.drawCursor(0,0)

    def increaseZoom(self):
        self.currentZoom = self.currentZoom + 2
        self.updateContents()


    def decreaseZoom(self):
        if(self.currentZoom > 2):
            self.currentZoom = self.currentZoom - 2
        else:
            self.currentZoom = 1
        self.updateContents()

    def createFrameWindow(self):
        with dpg.window(tag="frame_window",label="Frame",pos=(0,50),horizontal_scrollbar=True):
            with dpg.menu_bar():
                 dpg.add_button(tag='sprite_button_zoom_in',label="+",callback=self.increaseZoom)
                 dpg.add_button(tag='sprite_button_zoom_out',label="-", callback=self.decreaseZoom)
            with dpg.drawlist(width=self.currentProject.currentFrame.width*self.currentZoom, height=self.currentProject.currentFrame.height*self.currentZoom, tag="frame_draw_list") as self.frameDrawList:
                self.updateContents()
            dpg.add_text(default_value='', tag='statbartext')
            with dpg.item_handler_registry(tag="frame_item_mouse_handler"):
                dpg.add_item_clicked_handler(button=dpg.mvMouseButton_Left, callback=self.drawPoint)
           
            dpg.bind_item_handler_registry("frame_draw_list", "frame_item_mouse_handler")
            with dpg.handler_registry(tag="frame_mouse_handler"):
                dpg.add_mouse_drag_handler(button=dpg.mvMouseButton_Left, callback=self.drawPoint)
                dpg.add_mouse_move_handler(callback=self.updateCursor)
    
    def updateCursor(self, sender, app_data, user_data):
        (x,y) = dpg.get_drawing_mouse_pos()

        # if outside draw list then don't show coordinates
        if x < 0 or y < 0 or x > dpg.get_item_width(item='frame_draw_list') or y > dpg.get_item_height(item='frame_draw_list'):
            return
        x = int(x/self.currentZoom)
        y = int(y/self.currentZoom)
        dpg.set_value(item='statbartext', value=f'x: {x} , y: {y}')
        dpg.configure_item("cursor_tag", points=[(x*self.currentZoom,y*self.currentZoom),((x+1)*self.currentZoom,y*self.currentZoom),((x+1)*self.currentZoom,(y+1)*self.currentZoom),(x*self.currentZoom,(y+1)*self.currentZoom)],color=self.currentProject.currentPalette.getColorForDearPyGui(),fill=self.currentProject.currentPalette.getColorForDearPyGui())
        
    
    def updatePalette(self, currentPalette: Palette):
        self.currentProject.currentPalette = currentPalette
        dpg.set_value("frame_image_"+str(self.currentProject.currentAnimation.getCurrentFrameIndex()), self.currentProject.currentFrame.getTransformedTextureData(self.currentProject.currentPalette))
        #exporter = PillowImageManipulator()
        #scaledData = exporter.scaleFrameForDearPyGui(self.currentProject.currentFrame,self.currentProject.currentPalette,self.currentZoom)
        dpg.set_value("frame_image_current", self.currentProject.currentFrame.getTransformedTextureData(self.currentProject.currentPalette))

    def updateFrame(self):
        dpg.set_value("frame_image_current", self.currentProject.currentFrame.getTransformedTextureData(self.currentProject.currentPalette))
    
    def createFrameImage(self):
        frame = self.currentProject.currentFrame
        with dpg.texture_registry(show=False):
                dpg.add_dynamic_texture(frame.width, frame.height, frame.getTransformedTextureData(self.currentProject.currentPalette), tag="frame_image_current")

    def __init__(self, project: Project) -> None:
        self.currentZoom = 10
        self.currentProject = project
        
        self.createFrameImage()
        self.createFrameWindow()