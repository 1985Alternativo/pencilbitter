import dearpygui.dearpygui as dpg

import Palette
from Project import Project

class PaletteWindow():

    def selectColor(self, sender, app_data, user_data):
        self.project.currentPalette.currentColorIndex = user_data
        dpg.configure_item("red_color_slider", default_value=self.project.currentPalette.getColorComponent(self.project.currentPalette.currentColorIndex,0))
        dpg.configure_item("green_color_slider", default_value=self.project.currentPalette.getColorComponent(self.project.currentPalette.currentColorIndex,1))
        dpg.configure_item("blue_color_slider", default_value=self.project.currentPalette.getColorComponent(self.project.currentPalette.currentColorIndex,2))

    def updateColor(self, sender, app_data, user_data):
        self.project.currentPalette.updateColor(self.project.currentPalette.currentColorIndex, user_data, app_data)
        dpg.configure_item("palette_color_"+str(self.project.currentPalette.currentColorIndex),default_value=self.project.currentPalette.getColorForDearPyGui())
        self.updatePaletteListener(self.project.currentPalette)

    def createPaletteWindow(self):
        with dpg.window(label="Palette", pos=(400,50),width=300, height=200):
            with dpg.group(horizontal=True):
                for i in range(0,8):
                    dpg.add_color_button(self.project.currentPalette.getColorForDearPyGui(i), tag="palette_color_"+str(i))
                    dpg.set_item_callback("palette_color_"+str(i), self.selectColor)
                    dpg.set_item_user_data("palette_color_"+str(i), i)
            with dpg.group(horizontal=True):
                for i in range(0,8):
                    dpg.add_color_button(self.project.currentPalette.getColorForDearPyGui(i+8), tag="palette_color_"+str(i+8))
                    dpg.set_item_callback("palette_color_"+str(i+8), self.selectColor)
                    dpg.set_item_user_data("palette_color_"+str(i+8), i+8)

            dpg.add_slider_int(label="Red",min_value=0,max_value=3, tag="red_color_slider",default_value=self.project.currentPalette.getColorComponent(self.project.currentPalette.currentColorIndex,0))
            dpg.set_item_callback("red_color_slider", self.updateColor)
            dpg.set_item_user_data("red_color_slider", 0)
            dpg.add_slider_int(label="Green",min_value=0,max_value=3, tag="green_color_slider", default_value=self.project.currentPalette.getColorComponent(self.project.currentPalette.currentColorIndex,1))
            dpg.set_item_callback("green_color_slider", self.updateColor)
            dpg.set_item_user_data("green_color_slider", 1)
            dpg.add_slider_int(label="Blue",min_value=0,max_value=3, tag="blue_color_slider", default_value=self.project.currentPalette.getColorComponent(self.project.currentPalette.currentColorIndex,2))
            dpg.set_item_callback("blue_color_slider", self.updateColor)
            dpg.set_item_user_data("blue_color_slider", 2)


    def __init__(self, project:Project, updatePaletteListener) -> None:
        self.project = project
        self.createPaletteWindow()
        self.updatePaletteListener = updatePaletteListener