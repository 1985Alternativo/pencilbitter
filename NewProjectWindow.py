import dearpygui.dearpygui as dpg

class NewProjectWindow():

    def createNewProject(self):
        dpg.configure_item("new_project_modal", show=False)
        self.newprojectlistener(self.width,self.height)
            
    def updateWidthValue(self, sender, appData, userData):
        self.width = appData

    def updateHeightValue(self, sender, appData, userData):
        self.height = appData

    def createNewProjectWindow(self):
        with dpg.window(label="New Project",id="new_project_modal",pos=(200,200),width=200, height= 150,modal=True,show=False):
            dpg.add_text("Set Size of new Sprite")
            dpg.add_separator()
            with dpg.group(horizontal=False):
                dpg.add_input_int(label="width",default_value=self.width, step=8, callback=self.updateWidthValue)
                dpg.add_input_int(label="height", default_value=self.height, step=8, callback=self.updateHeightValue)
            with dpg.group(horizontal=True):
                dpg.add_button(label="OK",width=75, callback=self.createNewProject)
                dpg.add_button(label="Cancel",width=75, callback=lambda x: dpg.configure_item("new_project_modal", show=False))

    def __init__(self, newprojectlistener) -> None:
        self.width = 32
        self.height = 32
        self.createNewProjectWindow()
        self.newprojectlistener = newprojectlistener
        