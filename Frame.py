

from Palette import Palette


class Frame():

    def getTransformedTextureData(self,palette:Palette) -> list:
        transformed_texture_data = []
        for color in self.texture_data:
            colortuple = palette.getColorForDearPyGui(color)
            transformed_texture_data.append(colortuple[0]/255)
            transformed_texture_data.append(colortuple[1]/255)
            transformed_texture_data.append(colortuple[2]/255)
            transformed_texture_data.append(colortuple[3]/255)
        return transformed_texture_data

    def getTransformedTextureDataForSaving(self,palette:Palette) -> list:
        transformed_texture_data = []
        for color in self.texture_data:
            colortuple = palette.getColorForDearPyGui(color)
            transformed_texture_data.append(colortuple[0])
            transformed_texture_data.append(colortuple[1])
            transformed_texture_data.append(colortuple[2])
            transformed_texture_data.append(colortuple[3])
        return transformed_texture_data

    def setPixel(self,x:int, y:int, value:int):
        pixelPosition = (y*self.width+x)
        self.texture_data[pixelPosition] = value

    def __init__(self, width:int, height:int) -> None:
        self.width = width
        self.height = height
        self.texture_data = []
        for i in range(0, self.width * self.height):
            self.texture_data.append(0)
