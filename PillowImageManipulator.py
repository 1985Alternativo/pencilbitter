from PIL import Image
from PIL import ImageColor
import Project
import Frame
import Palette

class PillowImageManipulator():

    def exportSpritesheet(self, filename, project: Project):
        frame_width = project.currentFrame.width
        frame_height = project.currentFrame.height
        numAnimations = len(project.animationList)
        longestanimation = 0 
        for animation in project.animationList:
            if(longestanimation < len(animation.frameList)):
                longestanimation = len(animation.frameList)
        spritesheet_width = longestanimation * frame_width
        spritesheet_height = numAnimations * frame_height
        zerocolor = project.currentPalette.getColorForDearPyGui(0)
        sprite_sheet = Image.new("RGB",[spritesheet_width,spritesheet_height],ImageColor.getrgb("rgb(" + str(zerocolor[0]) + "," + str(zerocolor[1]) + "," + str(zerocolor[2]) + ")"))
        for animationindex,animation in enumerate(project.animationList):
            for frameindex,frame in enumerate(animation.frameList):
                texture_data = frame.getTransformedTextureDataForSaving(project.currentPalette)
                for y in range(0, frame_height):
                    for x in range(0,frame_width):
                        texture_data_index = ((y*frame_width)+x)*4
                        texture_data_tuple = (texture_data[texture_data_index],texture_data[texture_data_index+1],texture_data[texture_data_index+2])
                        texture_coordinate_tuple = (frame_width*frameindex + x,frame_height*animationindex + y)
                        sprite_sheet.putpixel(texture_coordinate_tuple, texture_data_tuple)
        sprite_sheet.save(filename)

    def importImage(self, filename) -> Project:
        imageDisk = Image.open(filename)
        if(imageDisk.width % 8 != 0):
                print("Width is not multiple of 8")
        if(imageDisk.height % 8 != 0):
                print("Height is not multiple of 8")
        project = Project.Project(imageDisk.width,imageDisk.height)
        imageDisk = imageDisk.quantize(colors=16,dither=Image.Dither.NONE)
        colors = imageDisk.palette.colors
        for color in colors.keys():
                print(color)
        #project.currentFrame
        return project
    
    def importSpritesheet(self, filename, project: Project):
        imagedisk = Image.open(filename)

    def scaleFrameForDearPyGui(self, frame: Frame.Frame, palette: Palette.Palette, zoom:int) -> list:
        frame_width = frame.width
        frame_height = frame.height
        scaled_width = frame_width*zoom
        scaled_height = frame_height*zoom
        original_frame = Image.new("RGB",[frame_width,frame_height])
        texture_data = frame.getTransformedTextureDataForSaving(palette)
        for y in range(0, frame_height):
            for x in range(0,frame_width):
                texture_data_index = ((y*frame_width)+x)*4
                texture_data_tuple = (texture_data[texture_data_index],texture_data[texture_data_index+1],texture_data[texture_data_index+2])
                texture_coordinate_tuple = (x,y)
                original_frame.putpixel(texture_coordinate_tuple, texture_data_tuple)
        resized_frame = original_frame.resize([scaled_width,scaled_height],resample=Image.Resampling.NEAREST)
        texture_data = []
        for y in range(0, scaled_height):
            for x in range(0,scaled_width):
                pixel_tuple = resized_frame.getpixel((x,y))
                texture_data.append(pixel_tuple[0]/255)
                texture_data.append(pixel_tuple[1]/255)
                texture_data.append(pixel_tuple[2]/255)
                texture_data.append(1)
        return texture_data