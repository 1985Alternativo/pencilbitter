import json
from tokenize import String
from Frame import Frame
from Palette import Palette
from Animation import Animation

class Project():
        def __init__(self,width:int, height:int) -> None:
                self.animationList = [Animation(width,height, "Animation 1")]
                self.currentAnimationIndex = 0
                self.currentAnimation:Animation = self.animationList[self.currentAnimationIndex]
                self.currentFrame:Frame = self.currentAnimation.getCurrentFrame()
                self.currentPalette:Palette = Palette()
                self.filePath = ""
        
        def addAnimation(self) -> None:
                newAnimation = Animation(self.currentFrame.width,self.currentFrame.height, "Animation " +str(len(self.animationList)+1))
                self.animationList.append(newAnimation)
        
        def deleteAnimation(self) -> None:
                animationToRemove = self.currentAnimation
                self.animationList.remove(animationToRemove)
        
        def cloneAnimation(self) -> None:
                newAnimation = Animation(self.currentFrame.width,self.currentFrame.height, self.currentAnimation.animationName + " copy")
                newAnimation.frameList = [] #Hacky but less changes to have it working properly
                for frame in self.currentAnimation.frameList:
                        newframe = Frame(frame.width,frame.height)
                        newframe.texture_data = frame.texture_data.copy()
                        newAnimation.frameList.append(newframe)
                currentIndex = self.currentAnimationIndex
                self.animationList.insert(currentIndex+1,newAnimation)

        def toJson(self) -> String:
                return json.dumps(self, default=lambda object: object.__dict__, sort_keys=True)