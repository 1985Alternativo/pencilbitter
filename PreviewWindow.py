import dearpygui.dearpygui as dpg

from Palette import Palette
from Project import Project
from Frame import Frame

class PreviewWindow():

    def playAnimation(self) -> None:
        self.playing = True
    def pauseAnimation(self) -> None:
        self.playing = False
    def stopAnimation(self) -> None:
        self.playing = False
        self.currentFrameIndex = 0
        self.updateFrame(self.currentFrameIndex)

    def addAnimation(self) -> None:
        print("Adding Animation")
        self.currentProject.addAnimation()
        dpg.configure_item("listbox_animations",items=self.currentProject.animationList)

    def deleteAnimation(self) -> None:
        print("Deleting Animation")
        self.currentProject.deleteAnimation()
        dpg.configure_item("listbox_animations",items=self.currentProject.animationList)


    def cloneAnimation(self) -> None:
        print("Cloning Animation")
        self.currentProject.cloneAnimation()
        dpg.configure_item("listbox_animations",items=self.currentProject.animationList)
    
    def animationSelected(self,sender) -> None:
        selectedAnimation = dpg.get_value(sender)
        for animationindex, animation in enumerate(self.currentProject.animationList):
            if animation.animationName == selectedAnimation:
                self.currentProject.currentAnimationIndex = animationindex
                self.currentProject.currentAnimation = animation
                self.currentFrameIndex = 0
                self.currentProject.currentFrame = self.currentProject.currentAnimation.getCurrentFrame()
                self.updateWindowListener()
                self.updateFrame()


    def createPreviewWindow(self) -> None:
        heightwindow = dpg.get_item_height("frame_window")
        heightwindow = 300 #fix
        index = self.currentProject.currentAnimation.currentFrameIndex
        with dpg.window(label="Preview", width=200, height=200,pos=(400,heightwindow)):
            with dpg.menu_bar():
                dpg.add_button(label="Play",callback=self.playAnimation)
                dpg.add_button(label="Pause",callback=self.pauseAnimation)
                dpg.add_button(label="Stop", callback=self.stopAnimation)
            with dpg.group(horizontal=True):
                dpg.add_listbox(self.currentProject.animationList,num_items=3, width=100,tag="listbox_animations",callback=self.animationSelected)
                dpg.add_button(label="+",callback=self.addAnimation)
                dpg.add_button(label="x",callback=self.deleteAnimation)
                dpg.add_button(label="oo", callback=self.cloneAnimation)
            with dpg.drawlist(width=self.currentProject.currentFrame.width*self.currentZoom, height=self.currentProject.currentFrame.height*self.currentZoom, tag="animation_draw_list") as self.frameDrawList:
                dpg.draw_image("frame_image_"+str(index),(0,0),(self.currentProject.currentFrame.width*self.currentZoom,self.currentProject.currentFrame.height*self.currentZoom))
            dpg.add_text(default_value=f'Current Animation:{self.currentProject.currentAnimationIndex}', tag='animationtext')
            dpg.add_text(default_value=f'Current Frame:{self.currentFrameIndex}', tag='frametext')    
            with dpg.item_handler_registry(tag="preview_visible_handler"):
                dpg.add_item_visible_handler(callback=self.animationCallback)
            dpg.bind_item_handler_registry("animation_draw_list", "preview_visible_handler")

    def animationCallback(self) -> None:
        if(self.playing):
                self.framesRendered = self.framesRendered + 1
                if(self.framesRendered > 60):
                        self.framesRendered = 0
                        self.currentFrameIndex = self.currentFrameIndex + 1
                        if(self.currentFrameIndex >= len(self.currentProject.currentAnimation.frameList)):
                                self.currentFrameIndex = 0
                        self.updateFrame(self.currentFrameIndex)
    
    def updateFrame(self, index=0) -> None:
        dpg.delete_item("animation_draw_list",children_only=True)
        dpg.draw_image("frame_image_"+str(index),(0,0),(self.currentProject.currentFrame.width*self.currentZoom,self.currentProject.currentFrame.height*self.currentZoom), parent=self.frameDrawList)
        dpg.set_value(item='animationtext', value=f'Current Animation:{self.currentProject.currentAnimationIndex}')
        dpg.set_value(item='frametext', value=f'Current Frame:{index}')
    
    def __init__(self, project: Project,updateWindowListener) -> None:
        self.currentProject = project
        self.currentZoom = 4
        self.currentFrameIndex = self.currentProject.currentAnimation.getCurrentFrameIndex()
        self.playing = False
        self.framesRendered = 0
        self.updateWindowListener = updateWindowListener
        self.createPreviewWindow()