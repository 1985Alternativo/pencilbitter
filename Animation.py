import string
from Frame import Frame

class Animation():
    def __init__(self, width:int, height:int, name:string) -> None:
        self.frameList = [Frame(width, height)]
        self.animationName = name
        self.currentFrameIndex = 0

    def __str__(self) -> string:
        return self.animationName
    
    def getCurrentFrame(self) -> Frame:
        return self.frameList[self.currentFrameIndex]

    def getCurrentFrameIndex(self) -> int:
        return self.currentFrameIndex

    def addFrame(self) -> Frame:
        width = self.getCurrentFrame().width
        height = self.getCurrentFrame().height
        self.frameList.append(Frame(width, height))
        return self.frameList[len(self.frameList)-1]

    def cloneCurrentFrame(self) -> Frame:
        width = self.getCurrentFrame().width
        height = self.getCurrentFrame().height
        currentIndex = self.currentFrameIndex
        newFrame = Frame(width,height)
        newFrame.texture_data = self.getCurrentFrame().texture_data.copy()
        self.frameList.insert(currentIndex + 1, newFrame)
        return self.frameList[currentIndex + 1]
